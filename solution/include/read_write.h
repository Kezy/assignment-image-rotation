//
// Created by Kezy2 on 19.11.2022.
//

#ifndef IMAGE_TRANSFORMER_READ_WRITE_H
#define IMAGE_TRANSFORMER_READ_WRITE_H

#include "image.h"

#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum write_status  {
    WRITE_OK,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );



//static struct bmp_header create_bmp_header(const struct image* img);
//
//static uint8_t padding(uint32_t width);
//
//static size_t file_size(const struct image* image);

#endif //IMAGE_TRANSFORMER_READ_WRITE_H
