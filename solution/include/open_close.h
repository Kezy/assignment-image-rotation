//
// Created by Kezy2 on 19.11.2022.
//

#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_OPEN_CLOSE_H
#define IMAGE_TRANSFORMER_OPEN_CLOSE_H

enum open_status {
    OPEN_OK,
    OPEN_ERROR
};

enum close_status {
    CLOSE_OK,
    CLOSE_ERROR
};

enum open_modes{
    READ_BIN = 0,
    WRITE_BIN = 1
};

enum open_status file_open(FILE** file, const char* name, enum open_modes mode);
enum close_status file_close(FILE* file);

#endif //IMAGE_TRANSFORMER_OPEN_CLOSE_H
