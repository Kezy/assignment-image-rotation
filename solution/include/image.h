//
// Created by Kezy2 on 19.11.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image { uint64_t width, height; struct pixel* data;};

struct image img_create_empty(uint64_t width, uint64_t height);


void free_img(struct image* image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
