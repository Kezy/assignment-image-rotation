//
// Created by Kezy2 on 16.01.2023.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

#include "image.h"

struct image rotate(const struct image src);

#endif //IMAGE_TRANSFORMER_TRANSFORM_H
