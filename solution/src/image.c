//
// Created by Kezy2 on 19.11.2022.
//

#include "../include/image.h"

#include <malloc.h>


struct image img_create_empty(uint64_t width, uint64_t height){
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(sizeof(struct pixel) * height * width);
    return img;
}


void free_img(struct image* image){
    free(image->data);
}
