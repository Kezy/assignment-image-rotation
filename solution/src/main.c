#include "../include/image.h"
#include "../include/open_close.h"
#include "../include/read_write.h"
#include "../include/transform.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3){
        fprintf(stderr, "- Not enough arguments \n");
        return 0;
    }

    FILE* file;
    if (file_open(&file, argv[1], READ_BIN) == OPEN_ERROR){
        fprintf(stderr, "- Failed to open input file \n");
        return 0;
    }

    struct image img = {0};
    enum read_status status = from_bmp(file, &img);
    if (status != READ_OK){
        fprintf(stderr, "- File read error \n");
        return 0;
    }
    file_close(file);


    struct image result_img = rotate(img);
    free_img(&img);

    FILE* result_file;
    if (file_open(&result_file, argv[2], WRITE_BIN) == OPEN_ERROR){
        fprintf(stderr, "- Failed to open output file \n");
        return 0;
    }

    if (to_bmp(result_file, &result_img) != WRITE_OK){
        fprintf(stderr, "- File write error \n");
        return 0;
    }
    file_close(result_file);
    free_img(&result_img);

    return 0;
}
