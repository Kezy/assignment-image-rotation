//
// Created by Kezy2 on 19.11.2022.
//

#include "../include/read_write.h"
#include "../include/image.h"


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

static uint8_t padding(uint32_t width){
    uint8_t result = 0;
    if (width % 4 != 0){
        result = 4 - ((width * 3) % 4);
    }
    return result;
}

static size_t file_size(const struct image* image){
    return image->height *
    (sizeof(struct pixel) * image->width + padding(image->width));
}

static struct bmp_header create_bmp_header(const struct image* img){
    struct bmp_header header;

    header.bfType = 19778;
    header.bfileSize = file_size(img) + sizeof(struct bmp_header);
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = file_size(img);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum read_status from_bmp(FILE* in, struct image* img){
    if (in == NULL){
        return READ_ERROR;
    }
    else {
        struct bmp_header file_header = {0};
        if (fread(&file_header, sizeof(struct bmp_header), 1, in)
                == 0) {
            return READ_INVALID_HEADER;
        }
        if (file_header.bfType != 19778){
            return READ_INVALID_SIGNATURE;
        }

        *img = img_create_empty(file_header.biWidth, file_header.biHeight);

        for (size_t i = 0; i < img->height; i++) {


            if (fread(&img->data[i * img->width], sizeof(struct pixel) * img->width, 1, in)
                == 0) {
                free_img(img);
                return READ_INVALID_BITS;
            }

            if (fseek(in, padding(img->width), SEEK_CUR) != 0) {
                free_img(img);
                return READ_INVALID_SIGNATURE;
            }
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img){
    struct bmp_header new_header = create_bmp_header(img);
    if (fwrite(&new_header, sizeof(struct bmp_header), 1, out) == 0){
        return WRITE_ERROR;
    }
    else{
        for (int64_t i = 0; i < img->height; i++){
//            for (int64_t j = 0; j < img->width; j++){
//                struct pixel pixel = img->data[i * img->width + j];
//                if (fwrite(&pixel, sizeof(pixel), 1, out) == 0){
//                    return WRITE_ERROR;
//                }
//            }
            if (fwrite(&img->data[i * img->width], sizeof(struct pixel) * img->width, 1, out) == 0){
                    return WRITE_ERROR;
                }
            int8_t bytes[] = {0, 0, 0, 0};

            if (fwrite(&bytes, sizeof(int8_t) * padding(img->width), 1, out) == 0){
                return WRITE_ERROR;
            }

        }
        return WRITE_OK;
    }
}
