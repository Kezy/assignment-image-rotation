//
// Created by Kezy2 on 16.01.2023.
//

#include "../include/image.h"
#include "../include/transform.h"


static struct image rotation_270(uint64_t w, uint64_t h, struct image src, struct image rotate_img){
    rotate_img.data[w * rotate_img.width + (rotate_img.width - h - 1)] = src.data[h * src.width + w];
    return rotate_img;
}

struct image rotate(const struct image src){
    struct image rotate_img = img_create_empty(src.height, src.width);

    for (uint64_t i = 0; i < src.height; i++){
        for (uint64_t j = 0; j < src.width; j++){
            rotate_img = rotation_270(j, i, src, rotate_img);
        }
    }

    return rotate_img;
}
