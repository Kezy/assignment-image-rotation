//
// Created by Kezy2 on 19.11.2022.
//
#include "../include/open_close.h"

#include <stdio.h>


static const char* modes[] = {
        "rb",
        "wb"
};

enum open_status file_open(FILE** file, const char* name, enum open_modes mode){
    *file = fopen(name, modes[mode]);
    if (*file == NULL){
        return OPEN_ERROR;
    }
    else{
        return OPEN_OK;
    }
}

enum close_status file_close(FILE* file){
    if (file == NULL){
        return CLOSE_ERROR;
    }
    else{
        return CLOSE_OK;
    }
}
